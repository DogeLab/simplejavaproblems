package com.traphouse.unitone;

/**
 * This problem will test your knowledge on integer and String arithmetic/operations.
 * @author carlos
 *
 */
public class ProblemTwo {

	public static void main(String[] args) {
		
		// The following are sample variables.
		int length = 10;
		int width = 5;
		int area;
		
		String computerName = "devTeamRed";
		String woodType = "Oak";
		
		/**
		 * For the following question, uncomment the following lines before reading the question.
		 * After answering the question, please comment the line again.
		 * Q: What value do you expect to be printed out?
		 * A:  
		 */
		//System.out.println(length + width);
		
		/**
		 * For the following question, uncomment the following lines before reading the question.
		 * After answering the question, please comment the line again.
		 * Q: What value do you expect to be printed out?
		 * A:  
		 */
		area = length * width;
		//System.out.println(area);
		
		/**
		 * Q: What is this operation called? (HINT: these variables are strings)
		 * A:  
		 */
		System.out.println(computerName + " - " + woodType);
		
		/**
		 * For the following question, uncomment the following lines before reading the question.
		 * After answering the question, please comment the line again.
		 * Q: Why does the un-commented line compile, but the commented line does not?
		 * A:
		 */
		computerName = computerName + length;
		//length = computerName + length;
		//test commit
		//second test

	}

}
