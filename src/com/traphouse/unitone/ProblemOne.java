package com.traphouse.unitone;

/**
 * This problem will test your knowledge on general variable concepts.
 * Some answers can be checked by following the instructions, and running the program,
 * others must be checked by @author carlos.
 * @author carlos
 *
 */
public class ProblemOne {

	public static void main(String[] args) {
		
		/*
		 * Q: Here, we are creating variables, but are not necessarily giving them a value.
		 * 	  What is this called?
		 * A: 
		 */
		int someNumber;
		String somePhrase;
		String anotherPhrase;
		
		/*
		 * Q: We are giving these variables values. What is this operation called?
		 * A: 
		 */
		someNumber = 5;
		somePhrase = "Yo";
		
		/*
		 * Q: What is the result of this operation (i.e. current value of someNumber)?
		 * A: 
		 */
		someNumber = someNumber + 5;
		
		/*
		 * Q1: What is the result of this operation (i.e. current value of somePhrase)?
		 * A:
		 * Q2: Is the current value of somePhrase still 'Yo'? If not, explain why.
		 * A: 
		 */
		somePhrase = "Ay lmao";
		
		// Uncomment the following lines and run the program to check your answers:
		//System.out.println(someNumber);
		//System.out.println(somePhrase);
		
		/*
		 * For the following question, uncomment the following lines before reading the question.
		 * After answering the question, please comment the line again.
		 * Q: Why does the following line result in a compile-time error?
		 * A: 
		 */
		//int someNumber = 7;
		
		/*
		 * For the following question, uncomment the following lines before reading the question.
		 * After answering the question, please comment the line again.
		 * Q: Why does the following line result in a compile-time error?
		 * A: 
		 */
		//System.out.println(anotherPhrase);
		
		/*
		 * Q: Should you create a variable and give it a value later, or do both at the same time?
		 * A:
		 * EX:
		 * 
		 * int someOtherNumber;
		 * someOtherNumber = 7;
		 * 
		 * OR
		 * 
		 * int someOtherNumbere = 7;
		 */

	}

}
